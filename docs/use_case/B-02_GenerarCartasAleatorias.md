# UseCase - Generar las cartas aleatorias

### Id #2

### Función: Generar las cartas aleatorias

### Descripcion:

A partir de varias colecciones de palabras, entre ellas conectores, 
adjetivos, etc. Se toman n cartas por cada tipo de palabras y se juntan en un
único mazo de cartas. De cada tipo de palabra, selecciona las palabras de forma
aleatoria.

### Precondicion

1. El mazo tenga cartas. 

### Poscondicion

1. Devuelve un mazo mezclado de forma aleatoria.

### Garantia de fallo

- El mazo esta vacio

### Requirimientos Especiales

- Texto - Simbolos indicando que se baraja.
  
### Prioridad 
Intermedia