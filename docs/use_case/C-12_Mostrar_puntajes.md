### Id: 12

### Funcion: Mostrar puntajes (Visual)

### Descripción

  Se trata de visualizar cuantas oraciones correctas tiene hasta el memento.
Ademas se visualizan las oraciones correctas hasta el momento. Tiene dos
formatos:

- Version corta
- Version larga

### Precondición
    S/A

### Poscondición
    S/A

### Garantía de fallo
    S/A

### Prioridad baja
    
### Requirimientos no funcionales - Requisitos especiales

Visualizacion completa
-----------------------
|         Pepe        |
| ------------------- | 
|  Puntos: 15         |
| ------------------- | 
|                     |
| Oraciones:          |
|                     |
| Esto es algo nuevo. |
|                     |
| Señor tiene una     |
|  moneda?            |
|                     |
| Mega oracion otra.  |
|                     |
| Salir        0      |
-----------------------

>> (Esto queda a la espera de la instruccion)



Version corta
-----------------------
|         Pepe        |
| ------------------- | 
|  Puntos: 15         |
| ------------------- | 
|                     |
| Oraciones:          |
|                     |
| Ultima oracion      |
| agregada.           |
|                     |
| Señor tiene una     |
|  moneda?            |
| ................... |
|                     | 
| Ver más...     1    |
|                     |
| Salir          0    | 
-----------------------

>> (Esto queda a la espera de la instruccion)