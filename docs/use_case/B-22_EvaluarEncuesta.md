### Id: 22

### Función: Evaluar encuesta

### Descripción:

Dada una coleccion de votos, suma la cantidad de votos a favor y los 
votos encontra. Una vez obtenidos se verifica cual es mayor, a favor o
en contra devolviendo un booleano:
- false: la sentencia no es aceptada
- true: sentencia aceptada

### Precondición

- Coleccion de votos

### Poscondición

 Se deja expresado si la encuesta es negativa o positiva.

### Relaciones con otros UseCases

- Agregar Voto
- Decisión del jugador
- Solicitar evaluacion de la oracion

### Garantía de fallo

### Prioridad: Intermedia

### Requirimientos no funcionales - Requisitos especiales