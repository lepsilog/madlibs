### Id: 29

### Función: Parámetros del juego. 

### Descripción:

Esta función se encarga darle modalidad al juego, es decir, distintas formas de
jugar al juego.

Modalidades:

- Con cronometro - Por defecto: No
- Indicar las cantidad de oraciones para ganar. Por defecto: 5
- cantidad de cartas por jugador. etc. Por defecto: 10
- cantidad de jugadores. etc. Por defecto: 2

### Precondición

- Ninguna

### Poscondición

- Parametros cambiados.

### Relaciones con otros UseCases

- jugarJuego()

### Garantía de fallo

- Ninguno

### Prioridad ALTA

### Requirimientos no funcionales - Requisitos especiales


Elegir reglas de juego.

------------------------------------------------------------------------------------
Seleccione la regla que desea modificar

1. Numero rondas para ganar.
2. Numero jugadores.
3. Cantidad cartas para armar oraciones (opcional) (por defecto diez cartas)
4. Con cronometro.
5. Salir
------------------------------------------------------------------------------------
    Numero rondas para ganar.
        Indique las rondas a ganar en numeros, presione 0 para salir. 
        POR DEFECTO 5 rondas

Las rondas se han modificado - No se modifico las rondas, queda por defecto (5).

    Numero jugadores.
        Indique la cantidad de jugadores, presione 0 para salir.
        Por defecto: 2 jugadores o 1 jugador

    Cantidad cartas para armar oraciones (opcional) 
    (por defecto diez cartas).
