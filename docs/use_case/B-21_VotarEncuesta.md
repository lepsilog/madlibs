### Id 21

### Función: Votar una encuesta

### Descripción:

  Dada una encuesta, se tiene que poder elegir entre si o no. El jugador, debera
visualizar la informacion de dicha encuesta.

### Parametros nesesarios:

- Encuesta
- Jugador

### Precondición:


### Poscondición:

La encuesta fue modifica, agregando un voto por si o por no.

### Relaciones con otros UseCases

- Enviar solicitud evaluacion
- Decicion del jugador
- Solicitar evaluacion a c/jugador

### Garantía de fallo

S/A

### Prioridad: Intermedia

### Requirimientos no funcionales - Requisitos especiales

--------------------------
| ¿Es una oracion        |
| correcta?              |
|                        |
| Oracion                |
|                        |
| Si (1)                 |
|                        |
| No (0)                 |
|                        |
--------------------------