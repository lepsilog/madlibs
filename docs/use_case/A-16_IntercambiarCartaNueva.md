### Id: 16

### Función: Intercambiar carta nueva

### Descripción: 

La función intercambia una carta del mazo propio por una recibida como argumento.
La carta recibida puede ser de mazo principal o descarte. 

### Precondición

- Carta recibida
- Tenga sentencia

### Poscondición

- Se agrego una carta nueva al mazo o sigue todo igual.

### Relaciones con otros UseCases

- elegirCartaDescartePrincipal() DeckManager

### Garantía de fallo

- Carta recibida

### Prioridad ALTA

### Requirimientos no funcionales - Requisitos especiales.

 ------    ------    ------   ------                 ------
|      |  |      |  |      | |      |               |      |
|      |  |      |  |      | |      |               |      |
 ------    ------    ------   ------                 ------