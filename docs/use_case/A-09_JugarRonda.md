### Id: #9

### Función: JugarRonda - Ronda

### Descripción: 

La función se encarga de la parte del comienzo de ronda y pasar de turnos 
entre los jugadores. Se utilizan dos funciones:

```
empezarTurno(jugadorActual);
terminarTurno(jugadorActual);

``` 
Pero antes de comenzar una ronda, se debe crear un manejador de cartas, que
es el encargado de repartirle guamazos a los jugadores(cartas) y crear dos masos
el de descarte y el principal, además de indicarle que jugar le toca de forma
secuencial.

El manejador de cartas se encargará de manejar al jugador en cuanto darle cartas
del mazo que quiera.

La función terminará cuando se determine algún ganador o hayan abandonado
el juego todos los jugadores. 

Utiliza funciones para determinar el ganador. En caso de que encuentre 
ganador llama a la parte grafica y muestra al ganador. No retorna nada.

Esta función utiliza SolicitarEvaluacionOracion() del jugador actual. 
Si es verdadero, le envia los datos necesarios a la funcion
VerificarGanadorOracion.

Esta función utiliza AbandonarJuego() del jSugador actual si desea salir 
del juego. Si es verdadero, Se eliminará el jugador del ¿conjunto?.

En caso de no haber ganador, continúa el siguente turno o jugador.

|Observación| : No es necesario devolver el jugador, ya que guarda 
a los ganadores en colección aparte, jugador - oracion ganadora

### UseCases internos

- SolicitarEvaluacionOracion()
- VerificarGanadorOracion()
- empezarTurno(jugadorActual)
- terminarTurno(jugadorActual);
- crearRepartidorCartas()
- repartirCartas()


### Relaciones con otros UseCases

- jugarJuego()
  
### Precondición

- S/A

### Poscondición

- S/A.

### Garantía de fallo

- La lista jugadores.
  
### Prioridad: ALTA

### Requirimientos no funcionales - Requisitos especiales

Cuando empieza la ronda (utiliza mostrarPuntajes())
--------------------------------------
|  Ronda: N                           |
--------------------------------------
|                                     |
| Jugadores:                          |
|                                     |
| blablabla        cantRondasGanadas  |
| blablabla 1:                        |
| blablabla ...:                      |
| blablabla N:                        |
--------------------------------------

Abandona jugador

--------------------------------------
|  el jugador ha salido del juego     |
--------------------------------------
|                                     |
|   -- DONDE ESTA TU HONOR BASURA --  |
|                                     |
--------------------------------------

Pasando turno, indicando siguiente jugador

--------------------------------------
|  el jugador es su turno            |
--------------------------------------


En caso en halla ganador de ronda
-------------------------
|  GANADOR: CHORIPAN    |
|                       |
| Rondas: N             |
|                       |
| Oraciones:            |
|                       |
| ORACION 1:            |
| ORACION ...:          |
| ORACION N:            |
-------------------------

