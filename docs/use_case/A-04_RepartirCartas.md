### Id: #4

### Función: Repartir cartas

### Descripción: 

La función recibe como argumento la colección de jugadores, el Mazo y 
cantidad de cartas para c/u.
Se selecciona la primer carta del Mazo.
Le saca una carta del Mazo y se lo agrega a cada jugador, se le 
agregará a su propio Mazo.


### Precondición

- Que existan jugadores, que se generen las cartas para el mazo.
- El mazo debe tener suficientes cartas para cada jugador.
- Definir cantidad de cartas para cada jugador.

### Poscondición

Al mazo se le quitaron una cierta cantidad de cartas. Es decir, 

n cantidad de cartas,
x cantidad de jugadores

n * x = cartas_quitadas

Las/Los jugadores se agregaron cartas. 

### Garantía de fallo

- Que falten cartas para repartir. 
- Que no tengan jugadores.
- La cantidad de jugadores sea mayor igual a 1.

### Prioridad (alta)

### Requirimientos no funcionales - Requisitos especiales

- Mostrar por consola que se está repartiendo.-