### Id 25

### Función: Pasar turno

### Descripción

  Esta funcion se encarga de limpiar el menu para el siguiente jugador. 

### Precondición

- Display

### Poscondición

El display queda limpio.

### Relaciones con otros UseCases

- Terminar turno

### Garantía de fallo

### Prioridad: Intermedia

### Requirimientos no funcionales - Requisitos especiales

----------------------
|                    |
| Siguiente          |
|     jugador..      |