### Id: 11

### Función: Cambiar entre menu

### Descripción:

Es una función que muestra la parte visual del juego,
indicando a los usuarios que menu aparecerá según la
opción elegida. Entre los menus se encuentran:

1. Menu de comienzo de juego.
2. Elegir reglas de juego. 
   1. Numero rondas para ganar.
   2. Numero jugadores.
   3. Cantidad cartas para armar oraciones (opcional) (por defecto diez cartas)

3. Nombre jugador.
4. Menu de jugador individual.
5. Menu de ordenar cartas.
6. Menu de cambiar descarte o principal.
7. Menu Mostrar jugador con sus oraciones y puntaje (finaliza juego).
8. Cambio entre jugadores.

### Precondición

- S/A

### Poscondición

- Cambio de estado

### Garantía de fallo

- Elegir una opción inválida

### Prioridad: Baja

### Requirimientos no funcionales - Requisitos especiales

[Menu general](noFuncional/index.md)


