### Id: 19

### Función: Decisión del jugador

### Descripción:

La función devuelve un voto si es favor o en contra. Recibe como argumento
un jugador y una sentencia (la oracion que se deberá evaluar). Dependiendo 
si es a favor o en contra. Se crea un voto con la decisión del jugador, una
vez creada se usara una función para agregar votos a una encuenta.


### Precondición

- Jugador 
- Sentencia

### Poscondición

- Se sumó un voto F/C

### Relaciones con otros UseCases

- agregarVoto() - Encuesta
- verificarGanadorOracion() - 

### Garantía de fallo

- La encuesta este vacia
- Error al elegir la opcion

### Prioridad MEDIA

### Requirimientos no funcionales - Requisitos especiales

