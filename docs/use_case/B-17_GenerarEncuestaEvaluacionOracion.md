### Id: 17

### Funcion: Generar encuesta evaluacion de la oracion

### Descripción:
  Genera una encuesta. Esta sera enviada a cada jugador, para
preguntarle si la oración es correcta, según su perspectiva.

Observacion: Cuando el jugador termina el turno, preguntar antes si hay una
encuesta de oracion.

### Precondición:

- La sentecia
- Jugador interesado 
- colJugadores * Se usa en la función de "enviarSolicitud(colJugadores)"

### Poscondición:

Se crea una encuesta nueva.

### Garantía de fallo
S/A

### Prioridad: Intermedia

### Requirimientos no funcionales - Requisitos especiales