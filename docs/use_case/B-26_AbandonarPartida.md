### Id: 26

### Función: Abandonar partida.

### Descripción:

Cuando un jugador desea abandonar el juego, cuando le corresponda el turno,
debe solicitarlo, es decir, elegir la opción "Abandonar".
Se debe cambiar la variable que almacena cada jugador a un estado verdadero
y luego es utilizada en otras opciones.
Además las cartas se agregarán al maso de descarte.

### Precondición

- jugadorActual
- variable abandonar este en falso.

### Poscondición

- solicitud de abandono de juego - abandonar es igual a verdadero

### Relaciones con otros UseCases

- jugarRonda() - Ronda
- terminarTurno() - Turno

### Garantía de fallo

- Que no exista el jugador.

### Prioridad MEDIA

### Requirimientos no funcionales - Requisitos especiales