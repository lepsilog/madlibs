# Diagrama clases

![archivo svg](./../img/ClassDiagram.svg)


![archivo png](./../img/diagramuml.png)


## Clases

| Madlibs     |

    -> configuraciones()
    -> generarCartasAleatorias()
    -> jugarJuego()

| Ronda     |

    -> var nro_ronda
    -> var turnos_jugados
    -> var lista_jugadores

    -> jugarRonda()
        *-> Mazo: barajarCartas()
        *-> Mazo: esVacio()
    -> pasarTurno()


| Turno        |

    -> var jugadorActual
    -> empezarturno()
    -> terminarturno(jugadores)


| Jugador     |
 
    -> var cantidad_rondas_ganadas
    -> col_oraciones_ganadoras
    -> var nombre
    -> var abandonar
    -> ManejadorSentencia misCartas
    -> var pedir_revision
    -> verMisCartas()
    -> solicitarRevision()
    -> guardarOracionGanador()
    -> AbandonarJuego()
    -> devolverCartas() // casoUso
    -> jugar(ManagerMazo) 
        Opciones
        ° Elegir carta mazo
        ° Elegir carta descarte
        ° Pasar turno.
        ° Abandonar.
        //devuelve

| Mazo        |

    -> var coleccion_cartas
    -> barajarCartas()
    -> agregarCarta(unaCarta) - 
    -> recuperarCarta() //Primera carta de PILA
    -> quitarCarta(unaCarta)
        ° menu con tus cartas y carta nueva 
        ° elegir o devolver
    -> esVacio()


| ManagerMazo |

    -> var mazo_principal
    -> var mazo_descarte 
    -> pedirCarta(eleccion) 
        ° solicita carta al mazo
        ° solicita carta al descarte
    -> repartirCartas(jugadores) 
    -> colocarCartaDescarte(unaCarta)

| Carta       |

    -> var Palabra
    -> var visible

    gráfica 
    ----------------
    |              | 
    | Tipo Palabra | 
    |              |
    |   PALABRA    |
    |              |
    -------------- |

| Sentencia |
    
    -> colCarta //
    -> mostrarSentencia()
    -> intercambiarConSentencia(unaCarta, Sentencia, unaCarta)
    -> intercambiar(unaCarta, unaCarta) //interno

    -> contains(nroCarta) : boolean
    -> quitarCarta(nroCarta) : boolean
    -> ordenarPorTipos()
    -> ponerCarta(pos, nroCarta)
    -> getIndex(nroCarta) : int

| ManejadorSentencia |

    -> order del: Sentencia
    -> disorder del: Sentencia
    -> mostrarSentenciaOrder()
    -> mostrarEspacioCartas()
    -> intercambiarInterno(cartaOrigen, cartaDestino)
    -> intercambiarEntreSentencias(cartaOrigen, cartaDestino)
        -> intercambiar de Ordenadas a desordenadas
    

| Carta |

    -> valor
    -> numero_carta
    -> tipo_palabra

| Tipo Carta |

    -> nombre

| Encuesta |

    -> Voto[] votosFavor
    -> Voto[] votosEnContra
    -> boolean esAceptada
    -> Sentencia oracion
    -> esAceptada()
    -> evaluarSentencia(colJugadores)
    -> agregarVoto(jugador, decision)

|Voto|

    -> decision - boolean
    -> nombreJugador
