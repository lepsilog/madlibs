# Desafios

## Madlibs 

## ¿De que va el juego?

Es un juego de frases. Se tiene una narración base de la cual se 
cambian algunas palabras. Con el fin de hacerlo más divertivo.

## ¿Cómo funciona?

Cada texto tendrá espacios en blanco para completarlos con palabras que 
nosotros querramos agregar. Que pueden ser Adj y verbos.

El juego consta de rondas. En cada ronda se elige la frase graciosa. Si un 
participante gana 3 concecutivas, es el que pierde.

# ¿Como se plantea realizarlo?

   Primeramente, se necesita una base de informacion, los textos. Luego, se 
tendria que ir analizando cada uno de ellos, para realizar marcas en los 
lugares a remplazar. El simbolo a utilizar seria "_"

  Como segunda medida, generar una funcion que analice cada marca del texto 
para luego asi mostrar en la pantalla. Se mostraria con varios guiones bajos
como si se dejara espacios en blanco para completar. Es mas que nada una 
referencia visual para el jugador.
(Tener cuidado a la hora de implementar en una interfaz grafica. Porque se 
tienen que generar los espacios de forma coherente a las marcas)

  La entrada de datos se toma como texto. El ingreso seria en el orden de 
aparicion.
(Es mas un detalle para la consola. Si tubiera una interfaz grafica
la cosa seria diferente.)