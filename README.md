# Madlibs 

Juego de palabras

## ¿De que trata el juego madlibs con cartas?

  Se trata de armar oraciones, atravez de las cartas que se tenga. Cada carta,
contiene una palabra y a que tipo pertenece.

## ¿Como funciona?

  Se utilizan cartas. Cada uno tiene 10 cartas. Existe un mazo. Cuando se arma
una oracion, se baja(Se muestra al resto). Una vez bajado no se puede modificar
la oracion creada.  
El minimo de jugadores son 2 hasta 58.

El mazo tiene 240 cartas. Se distribuyen en:  
* 48 sustantivos
* 48 verbos
* 48 advervios
* 48 adjetivos  
* 48 conectores  

Al repartir las cartas(unica vez), se deja la carta de arriba visible.
Cada jugador, saca un carta del mazo segun el turno que le toque.

¿Que pasa si un jugador no pudo formar una oracion?
Pierde.

### ¿Como se toman los puntos?  

El juego termina cuando un jugador llega tope de rondas solicitadas. La 
cantidad de rondas se define al comienzo de la partida.  
Si el mazo se queda sin cartas, se barajan las cartas de descartes. En este caso, 
la primera carta de arriba queda dada vuelta. Y se continua el juego.

Cuando un jugador arma una oracion, gana un punto. Se vuelve a barajar y 
repartir las cartas dejando la primera dada vuelta.

Para evaluar si una oracion es correcta, se le preguntara a todos los 
jugadores. En caso de empate, se toma como no correcta.

## Funciones de la aplicacion ¿Que funciones se nesecitan?

### ¿Como es la secuencia del juego?

0. Elegir cantidad de oraciones
1. Se tiene un mazo (principal).
2. Se barajan las cartas.
3. Se reparten las cartas (10 a cada uno).
4. Se deja una carta visible al principio del mazo
5. El primer jugador comienza el juego

6. Elegir una carta  
6a. Elegir una carta del descarte (cara arriba)  
6b. Elegir una carta del mazo (cara para abajo)  

7. En caso de armar oracion, mostrar cartas.  
8. Jugadores revisan oracion. Si es correcta ir a punto 10  
9. Dejar carta que no sirva, en el maso de descarte.  

10. Formas de ganar  
10a. Jugador llegar al tope de oraciones.  
10b. Jugador sin cartas (ganador)*  
10c. (Mazo sin cartas) Mezclar cartas descartes  

11. El juego se repite desde el punto 6. En caso de ganar, devolver ganador.  


###  Funcion principal
  Por consola. Tendria un menu:

  |Cartas propias| |mostrar carta primera del mazo descarte| 
  
  - Elegir carta mazo
  - Elegir carta descarte
  - Pasar turno.
  - Abandonar.


### ¿Como es la interaccion con el usuario?

Se tiene un menu cada vez que se elige una carta. De la siguiente forma:

¿Que desea hacer?
1. Tomar carta
2. Dejar carta (mazo descarte). (Sale de este menu y pasa el turno)
3. Ordenar mis cartas
4. Evaluar oracion
5. Pasar turno
6. Abandonar

El item 1. tiene un submenu de la siguiente forma:
cartas propias    | carta nueva

0. Intercambiar carta de mi mazo



### ¿Que funciones tendriamos que hacer?

01. [x] Barajar cartas  
02. [x] Generar las cartas aleatorias  
03. [x] Detectar si el mazo principal esta vacio  
04. [x] Repartir las cartas  
05. [x] Verificar ganador de la oracion  
06. [x] Verificar si sigue ronda  
07. [x] Verificar ganador de la Juego  
08. [x] Mostrar ganador  
09. [x] Jugar Ronda  
10. [x] Cambiar entre menus (No funcional)  
11. [x] Mostrar puntajes actuales (Generales) (no-funcional)  
12. [x] Mostrar cartas propias funcional y no funcional.  
13. [x] Limpiar pantalla  
14. [x] EnviarCartaDescarte  
15. [x] Ordenar mis cartas (Intercambiar cartas propias entre si)  
16. [x] Intercambiar por carta nueva  (deckManager)  
17. [x] Generar encuesta evaluacion de la oracion (round)  
18. [x] Enviar solicitud evaluacion a cada jugador  
19. [x] Decisión del jugador  
20. [x] Agregar Voto a la encuesta   
21. [x] Votar encuesta   
22. [x] Evaluar encuesta  
23. [x] Empezar turno  
24. [x] Terminar turno  
25. [x] Pasar turno
26. [x] Abandonar partida  
27. [x] Elegir una carta del mazo  
28. [x] Elegir una carta del mazo de descarte  
29. [x] Tomar la cantidad de oraciones y jugadores (parametros del juego)  
30. [x] Intercambiar entre sentencias  
31. [ ] Jugar juego  


### ¿Como se tiene que evaluar una oracion?

Cada jugador resivira una encuesta por si o no. Donde aparece la oracion que
se quiere evaluar.  

## Requerimientos funcionales y no funcionales

Estos estan detallados en la carpeta UseCase.
Se cuenta con un archivo plantilla para agilizar el proceso de llenado.

Ademas, en el directorio de casos de usos, se le asigna una prioridad. Esta es 
establecida con 3 letras:  
A : Urgente  
B : Intermedio  
C : Puede esperar  

[Plantilla de case use](./docs/use_case/generico.md)


## Diagramas


[Colocados los diseños de la aplicacion](./docs/diagrams/texts/UML.md)

## Fuentes

[reglas](https://www.usroasterie.com/tarjeta-de-cardenal-mad-libs-juego-reglas-del-juego.html)

[Sustantivos, verbos, adjetivos, advervios](https://gottamentor.com/mad-libs-game-word-lists)

[Conectores](https://www.ejemplos.co/100-ejemplos-de-conectores/)
